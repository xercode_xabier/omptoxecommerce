<?php

/*
 * This file is part of the xeBook package.
 *
 * (c) Xercode
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Service;

use App\eCommerce\CategoriesRepository;
use App\OAI\Model\Set;
use App\OAI\SetRepository;
use Psr\Log\LoggerInterface;

final class SyncSetsService
{
    private const codePrefix = 'omp_usc_set_%s';
    private const defaultParentCode = 'root_sets';
    /** @var SetRepository */
    private $setRepository;
    /** @var CategoriesRepository */
    private $categoriesRepository;
    /** @var LoggerInterface */
    private $logger;

    public function __construct(SetRepository $setRepository, CategoriesRepository $categoriesRepository, LoggerInterface $logger)
    {
        $this->setRepository    = $setRepository;
        $this->categoriesRepository = $categoriesRepository;
        $this->logger = $logger;
    }

    public function sync():void
    {
        $sets  = $this->setRepository->findAll();
        foreach ($sets as $set) {
            $this->createOrUpdateSet($set);
        }
    }

    private function createOrUpdateSet(Set $set):void
    {
        try {
            $code        = sprintf(self::codePrefix, $set->identifier);
            $parentCode  = self::defaultParentCode;
            $id          = $this->categoriesRepository->findCategoryIdByCode($code);

            if (null === $id) { // create
                $this->logger->info('Update Set.',  [
                    'code'         => $code,
                    'parent_code'  => $parentCode,
                    'setSpec'      => $set->setSpec
                ]);

                $this->categoriesRepository->addCategory(
                    $code,
                    $parentCode,
                    $set->textSpanish,
                    $set->textGalician,
                    $set->textEnglish,
                    $set->descriptionSpanish,
                    $set->descriptionGalician,
                    $set->descriptionEnglish
                );
                return;
            }

            $this->logger->info('Update Set.',  [
                'code'         => $code,
                'parent_code'  => $parentCode,
                'setSpec'      => $set->setSpec
            ]);

            // update
            $this->categoriesRepository->updateCategory(
                $code,
                $parentCode,
                $set->textSpanish,
                $set->textGalician,
                $set->textEnglish,
                $set->descriptionSpanish,
                $set->descriptionGalician,
                $set->descriptionEnglish
            );

        } catch (\Exception $exception) {
            $this->logger->error($exception);
        }
    }

}
