<?php

/*
 * This file is part of the xeBook package.
 *
 * (c) Xercode
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\OAI\Model;

final class Subject
{
    /** @var integer */
    public $identifier;
    /** @var string */
    public $code;
    /** @var integer */
    public $parentId;
    /** @var string */
    public $textEnglish;
    /** @var string */
    public $textSpanish;
    /** @var string */
    public $textGalician;

    public function __construct(
        int $identifier,
        string $code,
        int $parentId,
        string $textEnglish,
        string $textSpanish,
        string $textGalician
    ) {
        $this->identifier   = $identifier;
        $this->code         = $code;
        $this->parentId     = $parentId;
        $this->textEnglish  = $textEnglish;
        $this->textSpanish  = $textSpanish;
        $this->textGalician = $textGalician;
    }


}
