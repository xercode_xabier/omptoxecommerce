<?php

/*
 * This file is part of the xeBook package.
 *
 * (c) Xercode
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\OAI;

use App\OAI\Model\Set;
use Phpoaipmh\Endpoint;

final class SetRepository
{
    /** @var Endpoint */
    private $endpoint;

    public function __construct(Endpoint $endpoint)
    {
        $this->endpoint   = $endpoint;
    }

    /**
     * Find all sets
     * @return Set[]|null
     */
    public function findAll():?array
    {

        $response = $this->endpoint->listSets();
        $sets     = [];
        foreach ($response as $set) {
            $identifier          = (int)$set->setSpec->attributes()->{'identifier'};
            $setSpec             =  (string)$set->{'setSpec'};
            $name                =  (string)$set->{'setName'};
            $metadataTexts       =  $set->setTitles;
            $descriptionMetadata =  $set->setDescription;
            $textEnglish         = '';
            $textSpanish         = '';
            $textGalician        = '';
            $descriptionEnglish  = '';
            $descriptionSpanish  = '';
            $descriptionGalician = '';

            if(!empty($descriptionMetadata)) {
                foreach ($descriptionMetadata as $element) {
                    $language = (string)$element->attributes()->{'language'};
                    if ('en_US' === $language) {
                        $descriptionEnglish = (string)$element;
                    } elseif ('es_ES' === $language) {
                        $descriptionSpanish = (string)$element;
                    } elseif ('gl_ES' === $language) {
                        $descriptionGalician = (string)$element;
                    }
                }
            }

            if(!empty($metadataTexts)) {
                foreach ($metadataTexts as $text) {
                    $language = (string)$text->attributes()->{'language'};
                    if ('en_US' === $language) {
                        $textEnglish = (string)$text;
                    } elseif ('es_ES' === $language) {
                        $textSpanish = (string)$text;
                    } elseif ('gl_ES' === $language) {
                        $textGalician = (string)$text;
                    }
                }
            }

            $sets[] = new Set($identifier, $setSpec, $name, $textEnglish, $textSpanish, $textGalician, $descriptionEnglish, $descriptionSpanish, $descriptionGalician);
        }

        if (empty($sets)) {
            return null;
        }

        return $sets;
    }
}
