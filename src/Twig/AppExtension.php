<?php

/*
 * This file is part of the xeBook package.
 *
 * (c) Xercode
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

final class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('canonicalize', [$this, 'canonicalizeString']),
        ];
    }

    /**
     * Canonical name for string
     *
     * @param string $string
     *
     * @return string
     */
    public function canonicalize(string $string, string $separador = '_')
    {
        if (null === $string) {
            return null;
        }

        $encoding = mb_detect_encoding($string);
        $result = $encoding
            ? mb_convert_case($string, MB_CASE_LOWER, $encoding)
            : mb_convert_case($string, MB_CASE_LOWER);
        $slug = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $result);
        $slug = preg_replace("/[\/_|+ -]+/", $separador, $slug);

        return  $slug;
    }
}
