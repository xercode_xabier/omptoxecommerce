<?php

/*
 * This file is part of the xeBook package.
 *
 * (c) Xercode
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\OAI;

use App\OAI\Model\Record;
use Phpoaipmh\Endpoint;
use Psr\Log\LoggerInterface;

final class RecordRepository
{
    /** @var Endpoint */
    private $endpoint;

    /** @var string */
    protected $metadataPrefix;

    public function __construct(Endpoint $endpoint, LoggerInterface $logger, string $metadataPrefix = 'oai_onix')
    {
        $this->endpoint       = $endpoint;
        $this->logger         = $logger;
        $this->metadataPrefix = $metadataPrefix;
    }


    public function findOneById(string $id):?Record
    {
        try {
            $response       = $this->endpoint->getRecord($id, $this->metadataPrefix);
            $responseRecord = $response->GetRecord->record;
            $header         = $responseRecord->header;
            $attributes     = $header->attributes();
            $identifier     = (string)$header->identifier;
            $monograph      = (string)$header->monograph;
            $dateStamp      = (string)$header->datestamp;
            $setSpec        = (string)$header->setSpec;
            $metadata       = $responseRecord->metadata->count() > 0 ? $responseRecord->metadata :null;

            $isDeleted = false;
            if ($attributes->count() > 0 && ((string)$attributes->{'status'}) === 'deleted') {
                $isDeleted = true;
            }

            return new Record($this->metadataPrefix, $identifier, $monograph, $dateStamp, $setSpec, $metadata, $isDeleted);

        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage(), ['trace' => $exception->getTraceAsString()]);
            return null;
        }
    }
}
