<?php

/*
 * This file is part of the xeBook package.
 *
 * (c) Xercode
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\OAI\Model;

final class Identifier
{
    /**
     * @var string
     */
    private $metadataPrefix;

    /**
     * @var string
     */
    private $identifier;

    /**
     * @var string
     */
    private $dateStamp;

    /**
     * @var string
     */
    private $set;

    /**
     * @var bool
     */
    private $deleted = false;

    /**
     * Identifier constructor.
     * @param string $metadataPrefix
     * @param string $identifier
     * @param string $dateStamp
     * @param string $set
     * @param bool $deleted
     */
    public function __construct(
        string $metadataPrefix,
        string $identifier,
        string $dateStamp,
        string $set,
        bool $deleted
    ) {
        $this->metadataPrefix = $metadataPrefix;
        $this->identifier     = $identifier;
        $this->dateStamp      = $dateStamp;
        $this->set            = $set;
        $this->deleted        = $deleted;
    }

    /**
     * @return string
     */
    public function getMetadataPrefix(): string
    {
        return $this->metadataPrefix;
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @return string
     */
    public function getDateStamp(): string
    {
        return $this->dateStamp;
    }

    /**
     * @return string
     */
    public function getSet(): string
    {
        return $this->set;
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->deleted;
    }
}
