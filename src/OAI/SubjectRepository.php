<?php

/*
 * This file is part of the xeBook package.
 *
 * (c) Xercode
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\OAI;

use App\OAI\Model\Set;
use App\OAI\Model\Subject;
use Phpoaipmh\Client;

final class SubjectRepository
{
    /** @var Client */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }


    /**
     * Find all sets
     * @return Subject[]|null
     */
    public function findAll():?array
    {
        $response = $this->client->request('GET', ['verb' => 'ListSubjects']);
        if (empty($response->ListSubject)) {
            return  null;
        }

        $subjects = [];
        foreach ($response->ListSubject->subject as $element) {
            $identifier     = (int)$element->{'subjectIdentifier'};
            $code           = (string)$element->{'subjectCode'};
            $parentId       = (int)$element->{'subjectParentId'};
            $metadataTexts  = $element->setText;
            $textEnglish  = '';
            $textSpanish  = '';
            $textGalician = '';

            if(!empty($metadataTexts)) {
                foreach ($metadataTexts as $text) {
                    $language = (string)$text->attributes()->{'locale'};
                    if ('en_US' === $language) {
                        $textEnglish = (string)$text;
                    } elseif ('es_ES' === $language) {
                        $textSpanish = (string)$text;
                    } elseif ('gl_ES' === $language) {
                        $textGalician = (string)$text;
                    }
                }
            }

            $subjects[] = new Subject($identifier, $code, $parentId, $textEnglish, $textSpanish, $textGalician);
        }

        return $subjects;
    }
}
