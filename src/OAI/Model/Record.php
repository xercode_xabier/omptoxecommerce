<?php

/*
 * This file is part of the xeBook package.
 *
 * (c) Xercode
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\OAI\Model;

final class Record
{
    const PREFIX = 'oai_';

    /** @var string */
    private $metadataPrefix;

    /** @var string */
    public $identifier;

    /** @var string */
    public $dateStamp;

    /** @var string */
    public $set;

    /** @var \SimpleXMLElement|null */
    public $metadata;

    /** @var bool */
    public $deleted = false;

    /** @var string */
    public $monograph;

    public function __construct(string $metadataPrefix, string $identifier, string $monograph, string $dateStamp, string $set, ?\SimpleXMLElement $metadata = null, bool $deleted = false)
    {
        $this->metadataPrefix = $metadataPrefix;
        $this->identifier = $identifier;
        $this->monograph   = $monograph;
        $this->dateStamp  = $dateStamp;
        $this->set        = $set;
        $this->metadata   = $metadata;
        $this->deleted    = $deleted;
    }

    public function getMetadataPrefix(): string
    {
        return $this->metadataPrefix;
    }


    public function getMetadataFormat(): string
    {
        return substr($this->metadataPrefix, strlen(self::PREFIX));
    }
}
