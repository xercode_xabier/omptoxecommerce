<?php

/*
 * This file is part of the xeBook package.
 *
 * (c) Xercode
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\eCommerce;


trait StringTrait
{
    /**
     * Canonical name for string
     *
     * @param string $string
     *
     * @param string $separador
     *
     * @return string
     */
    protected function canonicalize(string $string, string $separador = '_')
    {
        if (empty($string)) {
            return null;
        }

        $encoding = \mb_detect_encoding($string);
        $result = $encoding
            ? \mb_convert_case($string, MB_CASE_LOWER, $encoding)
            : \mb_convert_case($string, MB_CASE_LOWER);
        $slug = \preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $result);
        $slug = \preg_replace("/[\/_|+ -]+/", $separador, $slug);

        return  $slug;
    }

    protected function stripTags(string $text):string
    {
        $result = preg_replace_callback(
            '/&lt;([\s\S]*?)&gt;/s',
            function ($matches) {
                return '';
            },
            $text
        );

        return htmlspecialchars_decode($result);
    }

    protected function truncate(string $text, int $width = 200):string
    {
        return \mb_strimwidth($text, 0, $width, "...");
    }

    private static $metaKeywordsBlacklist = 'a|sobre|encima|después|nuevamente|contra|todo|soy|un|y|ninguno|son|no|cuando|en|estar|porque|sido|antes|siendo|debajo|entre|ambos|pero|por|puede|podía|hizo|hacer|hace|haciendo|bajo|durante|cada|alguno|para|desde|más|tuvo|tiene|haber|habiendo|él|aquí|suyo|misma|su|mismo|cómo|si|en|dentro|es|eso|dejar|me|mayoría|mi|mismo|ni|desactivado|activado|solo|o|otro|nuestro|nuestros|mismos|fuera|propio|mismo|ella|debería|tal|que|el|sus|entonces|allí|estos|ellos|esos|aquellos|través|demasiado|hasta|arriba|muy|era|éramos|qué|cuándo|dónde|mientras|quién|con|de';

    protected function buildName(string $name):string
    {
        return $this->stripTags($name);
    }

    protected function buildMetaKeywords(string $description):string
    {
        $blacklist     = explode('|', self::$metaKeywordsBlacklist);
        $keywords      = explode(' ', $description);
        $numberOfWords = \count($keywords);

        for ($i = 0; $i < $numberOfWords; $i++) {
            $keyword = trim($keywords[$i]);
            if(empty($keyword)) {
                unset($keywords[$i]);
            } elseif (in_array($keyword, $blacklist, true)) {
                unset($keywords[$i]);
            }
        }

        return  implode(',', $keywords);
    }


    protected function buildMetaDescription(string $description):string
    {
        return $this->truncate($this->stripTags($description));
    }
}
