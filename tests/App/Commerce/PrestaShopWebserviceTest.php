<?php

/*
 * This file is part of the xeBook package.
 *
 * (c) Xercode
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Tests\App\Commerce;

use App\eCommerce\PrestaShopWebservice;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;

class PrestaShopWebserviceTest extends TestCase
{
    /** @var PrestaShopWebservice */
    private $webService;

    protected function setUp()
    {
        parent::setUp();
        $twig = new \Twig\Environment(new \Twig\Loader\FilesystemLoader(__DIR__.'/../../../templates'));
        $this->webService = new PrestaShopWebservice(
            $_ENV['XECOMMERCE_USC_HOST'],
            $_ENV['XECOMMERCE_USC_KEY'],
            $twig,
            new NullLogger(),
            true
        );
    }
    public function testFindCategoryIdExits()
    {
        $id = $this->webService->findCategoryIdByCode('e3989c6dedd9b824106b734e68dccaad');
        $this->assertNotNull($id);
        $category = $this->webService->findCategoryById($id);
        $this->assertNotNull($category);
    }

    public function testFindCategoryIdNotExits()
    {
        $id = $this->webService->findCategoryIdByCode('aaa');
        $this->assertNull($id);
    }

    public function testAddCategory()
    {
        $xml = $this->webService->addCategory(
            12,
            'demo',
            'nameES',
            'nameGL',
            'nameEN',
            'descriptionES',
            'descriptionGL',
            'descriptionEN'
        );

        $this->assertNotNull($xml);
    }

    /**
     * @test
     */
    public function updateCategory()
    {
        $xml = $this->webService->updateCategory(
            12,
            'demo',
            'nameUpdateCategoryES',
            'nameUpdateCategoryGL',
            'nameUpdateCategoryEN',
            'descriptionUpdateCategoryES',
            'descriptionUpdateCategoryGL',
            'descriptionUpdateCategoryEN'
        );

        $this->assertNotNull($xml);
    }
}
