<?php

/*
 * This file is part of the xeBook package.
 *
 * (c) Xercode
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Service;

use App\eCommerce\CategoriesRepository;
use App\OAI\Model\Subject;
use App\OAI\SubjectRepository;
use Psr\Log\LoggerInterface;

final class SyncSubjectsService
{
    private const codePrefix = 'omp_usc_subject_%s';
    private const defaultParentCode = 'root_subjects';
    /** @var SubjectRepository */
    private $subjectRepository;
    /** @var CategoriesRepository */
    private $categoriesRepository;
    /** @var LoggerInterface */
    private $logger;

    public function __construct(SubjectRepository $subjectRepository, CategoriesRepository $categoriesRepository, LoggerInterface $logger)
    {
        $this->subjectRepository    = $subjectRepository;
        $this->categoriesRepository = $categoriesRepository;
        $this->logger = $logger;
    }

    public function sync():void
    {
        $subjects  = $this->subjectRepository->findAll();

        foreach ($subjects as $subject) {
            $this->createOrUpdateSubject($subject);
        }
    }

    private function createOrUpdateSubject(Subject $subject): void
    {

        try {

            $code        = sprintf(self::codePrefix, $subject->identifier);
            $parentCode = sprintf(self::codePrefix, $subject->parentId);
            if (0 === $subject->parentId) {
                $parentCode = self::defaultParentCode;
            }

            $id = $this->categoriesRepository->findCategoryIdByCode($code);
            if (null === $id) { //create new
                $this->logger->info('Create Subject.',  [
                    'code'         => $code,
                    'parent_code'  => $parentCode,
                    'setSpec'      => $subject->textSpanish
                ]);
                $this->categoriesRepository->addCategory(
                    $code,
                    $parentCode,
                    $subject->textSpanish,
                    $subject->textGalician,
                    $subject->textEnglish
                );
                return;
            }

            // update
            $this->logger->info('Update Subject.',  [
                'code'         => $code,
                'parent_code'  => $parentCode,
                'setSpec'      => $subject->textSpanish
            ]);

            $this->categoriesRepository->updateCategory(
                $code,
                $parentCode,
                $subject->textSpanish,
                $subject->textGalician,
                $subject->textEnglish
            );


        } catch (\Exception $exception) {
            $this->logger->error($exception);
        }


    }
}
