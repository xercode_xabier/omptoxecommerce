<?php

/*
 * This file is part of the xeBook package.
 *
 * (c) Xercode
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\eCommerce;

use SimpleXMLElement;
use Twig\Environment as Twig;
use Psr\Log\LoggerInterface;

final class FeatureRepository
{
    /** @var PrestaShopWebservice */
    private $webService;
    /** @var Twig */
    private $twig;
    /** @var LoggerInterface */
    private $logger;

    public function __construct(PrestaShopWebservice $prestaShopWebservice, Twig $twig, LoggerInterface $logger)
    {
        $this->webService = $prestaShopWebservice;
        $this->twig       = $twig;
        $this->logger     = $logger;
    }


    public function findFeatureIdByCode(string $code):?int
    {
        $response =  $this->webService->get(
            [
                'resource'            => 'product_features',
                'filter[xebook_code]' => $code,
            ]
        );

        $features = $response->product_features;
        if (!property_exists($features, 'product_feature')) {
            return null;
        }

        return (int)$features->product_feature->attributes()->id;
    }
    public function findFeatureById(int $featureId):?SimpleXMLElement
    {
        return $this->webService->get(
            [
                'resource' => 'product_features',
                'id'       => $featureId,
            ]
        );
    }
    public function findFeatureByCode(string $code):?SimpleXMLElement
    {
        $id = $this->findFeatureIdByCode($code);
        if (null == $id) {
            throw new PrestaShopWebserviceException(sprintf('Feature with code %s not found.', $code));
        }

        return $this->findFeatureById($id);
    }
    public function findFeatureValueIdByCode(string $code):?int
    {
        $response = $this->webService->get(
            [
                'resource'            => 'product_feature_values',
                'filter[xebook_code]' => $code,
            ]
        );

        $featureValues = $response->product_feature_values;
        if (!property_exists($featureValues, 'product_feature_value')) {
            return null;
        }

        return (int)$featureValues->featureValues->attributes()->id;
    }
    public function findFeatureValueById(int $featureValueId):?SimpleXMLElement
    {
        return $this->webService->get(
            [
                'resource' => 'product_feature_values',
                'id'       => $featureValueId,
            ]
        );
    }
    public function findFeatureValueByCode(string $code):?SimpleXMLElement
    {
        $id = $this->findFeatureValueIdByCode($code);
        if (null == $id) {
            throw new PrestaShopWebserviceException(sprintf('Feature value with code %s not found.', $code));
        }

        return $this->findFeatureValueById($id);
    }
    public function addFeatureValue(
        int $featureId,
        string $code,
        string $nameInSpanish,
        string $nameInGalician,
        string $nameInEnglish
    ): SimpleXMLElement {
        $postXml = $this->twig->render('product_feature_values.xml.twig', [
            'featureId'             => $featureId,
            'code'                  => $code,
            'nameInSpanish'         => $nameInSpanish,
            'nameInGalician'        => $nameInGalician,
            'nameInEnglish'         => $nameInEnglish

        ]);

        return $this->webService->add(
            [
                'resource' => 'product_feature_values',
                'postXml'  => $postXml,
            ]
        );
    }

    public function updateFeatureValue(
        string $code,
        string $nameInSpanish,
        string $nameInGalician,
        string $nameInEnglish
    ): ?SimpleXMLElement {

        $xml = $this->findFeatureValueByCode($code);

        if (null == $xml) {
            $this->logger->error(sprintf('The Feature value with code %s not found.', $code), [
                [
                    'code'          => $code,
                    'nameInSpanish' => $nameInSpanish,
                ],
            ]);

            return null;
        }

        $xml->product_feature_value->value->language[0] = $nameInSpanish;
        $xml->product_feature_value->value->language[1] = $nameInGalician;
        $xml->product_feature_value->value->language[2] = $nameInEnglish;

        $putXml = $xml->asXML();

        return $this->webService->edit(
            [
                'resource' => 'product_feature_values',
                'id'       => (int)$xml->product_feature_value->id,
                'putXml'   => $putXml,
            ]
        );
    }
}
