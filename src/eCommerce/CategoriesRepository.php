<?php

/*
 * This file is part of the xeBook package.
 *
 * (c) Xercode
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\eCommerce;

use Psr\Log\LoggerInterface;
use SimpleXMLElement;
use Twig\Environment as Twig;

final class CategoriesRepository
{
    use StringTrait;

    private const subject_default_parent_id = 12;
    private const set_default_parent_id     = 12;

    /** @var PrestaShopWebservice */
    private $webService;
    /** @var Twig */
    private $twig;
    /** @var LoggerInterface */
    private $logger;

    public function __construct(PrestaShopWebservice $prestaShopWebservice, Twig $twig, LoggerInterface $logger)
    {
        $this->webService = $prestaShopWebservice;
        $this->twig       = $twig;
        $this->logger     = $logger;
    }


    public function findCategoryById(int $categoryId): ?SimpleXMLElement
    {
        return $this->webService->get(
            [
                'resource' => 'categories',
                'id'       => $categoryId,
            ]
        );
    }

    public function findCategoryIdByCode(string $code): ?int
    {
        $response = $this->webService->get(
            [
                'resource'            => 'categories',
                'filter[xebook_code]' => $code,
            ]
        );

        $categories = $response->categories;
        if (!property_exists($categories, 'category')) {
            return null;
        }

        return (int)$categories->category->attributes()->id;

    }

    public function findCategoryByCode(string $code): ?SimpleXMLElement
    {
        $id = $this->findCategoryIdByCode($code);
        if (null == $id) {
            throw new PrestaShopWebserviceException(sprintf('Category with code %s not found.', $code));
        }

        return $this->findCategoryById($id);

    }

    public function addCategory(
        string $code,
        string $parentCode,
        string $nameInSpanish,
        string $nameInGalician,
        string $nameInEnglish,
        ?string $descriptionInSpanish = null,
        ?string $descriptionInGalician = null,
        ?string $descriptionInEnglish = null
    ): SimpleXMLElement {

        $parent = $this->findCategoryByCode($parentCode);

        $parameters = [
            'parentId'                  => (int)$parent->category->id,
            'code'                      => $code,
            'nameInSpanish'             => $this->buildName($nameInSpanish),
            'nameInGalician'            => $this->buildName($nameInGalician),
            'nameInEnglish'             => $this->buildName($nameInEnglish),
            'linkRewriteInSpanish'      => $this->canonicalize($nameInSpanish),
            'linkRewriteInGalician'     => $this->canonicalize($nameInGalician),
            'linkRewriteInEnglish'      => $this->canonicalize($nameInEnglish),
            'metaKeywordsInSpanish'     => $this->buildMetaKeywords($nameInSpanish),
            'metaKeywordsInGalician'    => $this->buildMetaKeywords($nameInGalician),
            'metaKeywordsInEnglish'     => $this->buildMetaKeywords($nameInEnglish),
        ];

        if (null !== $descriptionInSpanish) {
            $parameters['descriptionInSpanish'] = $descriptionInSpanish;
            $parameters['metaDescriptionInSpanish'] = $this->buildMetaDescription($descriptionInSpanish);
        } else {
            $parameters['descriptionInSpanish'] = '';
            $parameters['metaDescriptionInSpanish'] = '';
        }


        if (null !== $descriptionInGalician) {
            $parameters['descriptionInGalician'] = $descriptionInGalician;
            $parameters['metaDescriptionInGalician'] = $this->buildMetaDescription($descriptionInGalician);
        } else {
            $parameters['descriptionInGalician'] = '';
            $parameters['metaDescriptionInGalician'] = '';
        }

        if (null !== $descriptionInEnglish) {
            $parameters['descriptionInEnglish'] = $descriptionInEnglish;
            $parameters['metaDescriptionInEnglish'] = $this->buildMetaDescription($descriptionInEnglish);
        } else {
            $parameters['descriptionInEnglish'] = '';
            $parameters['metaDescriptionInEnglish'] = '';
        }

        $postXml = $this->twig->render('categories.xml.twig', $parameters);

        return $this->webService->add(
            [
                'resource' => 'categories',
                'postXml'  => $postXml,
            ]
        );
    }

    public function updateCategory(
        string $code,
        string $parentCode,
        string $nameInSpanish,
        string $nameInGalician,
        string $nameInEnglish,
        ?string $descriptionInSpanish = null,
        ?string $descriptionInGalician = null,
        ?string $descriptionInEnglish = null
    ): ?SimpleXMLElement {

        $xml    = $this->findCategoryByCode($code);
        $parent = $this->findCategoryByCode($parentCode);

        if (null == $xml) {
            $this->logger->error(sprintf('The Category with code %s not found.', $code), [
                [
                    'parentId'      => $parentCode,
                    'code'          => $code,
                    'nameInSpanish' => $nameInSpanish,
                ],
            ]);

            return null;
        }

        $currentXml = $xml->asXML();

        $xml->category->id_parent         = (int)$parent->category->id;
        $xml->category->name->language[0] = $this->buildName($nameInSpanish);
        $xml->category->name->language[1] = $this->buildName($nameInGalician);
        $xml->category->name->language[2] = $this->buildName($nameInEnglish);

        $xml->category->link_rewrite->language[0] = $this->canonicalize($nameInSpanish);
        $xml->category->link_rewrite->language[1] = $this->canonicalize($nameInGalician);
        $xml->category->link_rewrite->language[2] = $this->canonicalize($nameInEnglish);

        $xml->category->meta_title->language[0] = $this->buildName($nameInSpanish);
        $xml->category->meta_title->language[1] = $this->buildName($nameInGalician);
        $xml->category->meta_title->language[2] = $this->buildName($nameInEnglish);

        $xml->category->meta_keywords->language[0] = $this->buildMetaKeywords($nameInSpanish);
        $xml->category->meta_keywords->language[1] = $this->buildMetaKeywords($nameInGalician);
        $xml->category->meta_keywords->language[2] = $this->buildMetaKeywords($nameInEnglish);


        if (null !== $descriptionInSpanish) {
            $xml->category->description->language[0]      = $descriptionInSpanish;
            $xml->category->meta_description->language[0] = $this->buildMetaDescription($descriptionInSpanish);
        } else {
            $xml->category->description->language[0]      = '';
            $xml->category->meta_description->language[0] = '';
        }

        if (null !== $descriptionInGalician) {
            $xml->category->description->language[1]      = $descriptionInGalician;
            $xml->category->meta_description->language[1] = $this->buildMetaDescription($descriptionInGalician);
        } else {
            $xml->category->description->language[1]      = '';
            $xml->category->meta_description->language[1] = '';
        }

        if (null !== $descriptionInEnglish) {
            $xml->category->description->language[2]      = $descriptionInEnglish;
            $xml->category->meta_description->language[2] = $this->buildMetaDescription($descriptionInEnglish);
        } else {
            $xml->category->description->language[2]      = '';
            $xml->category->meta_description->language[2] = '';
        }

        $updateXml = $xml->asXML();

        $diff = \strcmp($currentXml, $updateXml);
        if (0 === $diff) {
            $this->logger->info(sprintf('Update Not %s equal to server. ', $code));
            return $xml;
        }

        unset($xml->category->level_depth);
        unset($xml->category->nb_products_recursive);

        $putXml = $xml->asXML();

        try {
            return $this->webService->edit([
                    'resource' => 'categories',
                    'id'       => (int)$xml->category->id,
                    'putXml'   => $putXml,
                ]
            );

        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage(), [
                'resource' => 'categories',
                'id'       => (int)$xml->category->id,
                'putXml'   => $putXml,
            ]);

            return null;
        }
    }
}
