<?php

/*
 * This file is part of the xeBook package.
 *
 * (c) Xercode
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Command\eCommerce;

use App\Service\SyncSubjectsService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

final class SyncSubjectsCommand extends Command
{
    protected static $defaultName = 'commerce:sync:usc:subjects';

    /** @var SyncSubjectsService */
    private $syncSubjectsService;

    public function __construct(SyncSubjectsService $syncSubjectsService)
    {
        $this->syncSubjectsService = $syncSubjectsService;
        parent::__construct(self::$defaultName);
    }

    protected function configure()
    {
        $this->setName(self::$defaultName);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Start sync subjects.');
        $this->syncSubjectsService->sync();
        $io->success('End sync subjects.');
        return 0;
    }
}
