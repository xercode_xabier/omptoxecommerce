<?php

/*
 * This file is part of the xeBook package.
 *
 * (c) Xercode
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\OAI\Model;

final class Set
{
    /** @var int */
    public $identifier;
    /** @var string */
    public $setSpec;
    /** @var string */
    public $setName;
    /** @var string */
    public $textEnglish;
    /** @var string */
    public $textSpanish;
    /** @var string */
    public $textGalician;
    /** @var string|null */
    public $descriptionEnglish;
    /** @var string|null */
    public $descriptionSpanish;
    /** @var string|null */
    public $descriptionGalician;

    public function __construct(
        int $identifier,
        string $setSpec,
        string $setName,
        string $textEnglish,
        string $textSpanish,
        string $textGalician,
        ?string $descriptionEnglish,
        ?string $descriptionSpanish,
        ?string $descriptionGalician
    ) {
        $this->identifier          = $identifier;
        $this->setSpec             = $setSpec;
        $this->setName             = $setName;
        $this->textEnglish         = $textEnglish;
        $this->textSpanish         = $textSpanish;
        $this->textGalician        = $textGalician;
        $this->descriptionEnglish  = $descriptionEnglish;
        $this->descriptionSpanish  = $descriptionSpanish;
        $this->descriptionGalician = $descriptionGalician;
    }


}

